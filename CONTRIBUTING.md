## Contributing

Contributions are welcome to fix any visual issues or to offer design suggestions.

Please submit any modifications as an SVG and include all extra assets in the Assets/ directory.