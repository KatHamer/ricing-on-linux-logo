<img src="./PNG/RicingOnLinux.png">

# Ricing on Linux Logo

Open source logo for https://t.me/RicingOnLinux

## Contributors

- [Katelyn](https://gitlab.com/KatHamer) - Initial logo concept and GIMP design
- [Amolith](https://gitlab.com/amolith) - Complete vector overhaul and misc cleanup
- [Erick](https://gitlab.com/zsucrilhos) - Background sourcing and design input

## Fonts used

- Logo text - [Gentona by Rene Bieder](https://www.dafontfree.io/gentona-font/)
- Monospaced code - [JetBrains Mono](https://www.jetbrains.com/lp/mono/)
